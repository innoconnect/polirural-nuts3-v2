WGL.ui.HeatMapBoxes = function(m, div_id, filterId, params) {

    let that = this;

    let h;
    let permalink_input;

    const CELL_SIZE = 14;
    const NUMBER_OF_COLORS = 6;

    this.activeFilters = [];

    if (typeof params === "undefined") {
        h = 150;
        permalink_input = null;
    } else {
        h = (params.h ? params.h : 180);
        permalink_input = (params.permalink_input ? params.permalink_input : null);
    }

    let selectedColors = ["#059445", "#609E43", "#D6AB41", "#E8833A", "#D14C33", "#C0222D"];
    let unselectedColors = [  [49/256, 130/256, 189/256],
        [158/256, 202/256, 225/256],
        [222/256, 235/256, 247/256]
    ];

    let formatColor = d3.scaleQuantize().domain([m.minCount, m.maxCount]).range(d3.range(NUMBER_OF_COLORS).map(d => d));

    const getUnselectedColor = function(val){
        let col1;
        let col2;
        let col;
        let rangeval;

        if (val >= 0.5){
            col1 = unselectedColors[0];
            col2 = unselectedColors[1];
            rangeval = (val - 0.5)*2.;

        } else {
            col1 = unselectedColors[1];
            col2 = unselectedColors[2];
            rangeval = val *2.;

        }

        col1 = col1.map(a => {
            return a * rangeval;
        });

        col2 = col2.map(a => {
            return a * (1 - rangeval);
        });

        col = col1.map((a, index) => {
            return a + col2[index];
        });

        return "rgb("+ (col[0] * 256) + "," + (col[1] * 256) + "," + (col[2] * 256) + ")";
    };

    this.getDivId = function () {
        return div_id;
    };

    this.init = () => {

        let div_hm = $("<div id='heatmap-boxes-parent' class='js-heatmap' style='width: 100%;'></div>");
        $("#" + div_id).append(div_hm);

        let dateStart = m.startDate;
        let dateEnd = m.endDate;

        let dateStartRange = m.startDateRange;
        let dateEndRange = m.endDateRange;

        let month_selection = $("<div class='month-selection' id='month-selection' >" +
            "<span style='position: relative'>" +
            "<span><select id='select-date' class='select-calendar'>" +
            "<option value=\"last-30-days\">Last 30 days</option>" +
            "</select></span>" +
            "</span>" +
            "</div>");
        $("#heatmap-boxes-parent").append(month_selection);

        let dateText;
        let dateValue;
        let dateStartMoment = moment(dateStart);
        let dateEndMoment = moment(dateEnd);
        while (dateEndMoment > dateStartMoment || dateStartMoment.format('M') === dateEndMoment.format('M')) {

            dateText = dateEndMoment.format('MMMM YYYY');
            dateValue = dateEndMoment.format('MM-YYYY');

            var o = new Option(dateText, dateValue);
            /// jquerify the DOM object 'o' so we can use the html method
            $(o).html(dateText);
            $("#select-date").append(o);

            dateEndMoment.subtract(1,'month');
        }

        let calendar_display =
                $("<div id='calendar-display' style='width: 100%;'></div>");
        $("#heatmap-boxes-parent").append(calendar_display);

        $("#heatmap-boxes-parent").append($("<div id='calendar-note' style='width: 100%'></div>"));

        $("#select-date").off("change").on("change", function() {
           let val = $(this).val();
           let lastMonth, lastYear;
           let startDate, endDate;

           if(val === 'last-30-days') {
               startDate = moment().subtract(31, 'days').format("YYYY-MM-DD");
               endDate = moment().format("YYYY-MM-DD");
           } else {
               lastMonth = parseInt(val.split("-")[0]);
               lastYear = parseInt(val.split("-")[1]);

               startDate = moment([lastYear, lastMonth-1]).startOf('month').format("YYYY-MM-DD");
               endDate = moment([lastYear, lastMonth-1]).endOf('month').format("YYYY-MM-DD");
           }

           delete $.data(document.body)['plugin_CalendarHeatmap'];

           reloadDateRange(new DataLoader(), startDate, endDate);
        });

        if(moment(dateStartRange).startOf('day').diff(moment().subtract(31, 'days').startOf('day')) === 0
            && moment(dateEndRange).startOf('day').diff(moment().startOf('day')) === 0) {
            $("#select-date").val('last-30-days');

            $("#calendar-display").CalendarHeatmap(m.data, {months: 1, displayLast30Days: true, labels: {days: true, months: false}, legend: {show: true}});

            var days_calendar = $(".ch-day:not(.is-outside-month)");
            for(var i=0; i<days_calendar.length; i++) {
                $(days_calendar[i]).append($("<text>"+(moment().subtract(30-i, 'days').date())+"</text>"));
                $(days_calendar[i]).addClass("ch-day-selected");
            }
        } else {

            let month = dateStartRange.getMonth();
            let year = dateStartRange.getFullYear();

            $("#select-date").val(("0" + (month+1)).slice(-2)+ "-" + year);

            $("#calendar-display").CalendarHeatmap(m.data, {months: 1, displayLast30Days: false, lastMonth: month+1, lastYear: year, labels: {days: true, months: false}, legend: {show: true}});

            var days_calendar = $(".ch-day:not(.is-outside-month)");
            for(var i=0; i<days_calendar.length; i++) {
                $(days_calendar[i]).append($("<text>"+(i+1)+"</text>"));
                $(days_calendar[i]).addClass("ch-day-selected");
            }
        }

        $(".is-outside-month:not(.is-outside-month-start)").remove();

        $(".ch-day:not(.is-outside-month-start):not(.ch-day-no-data)").off("click").on("click", e => {

            let target = e.target;
            if(!target.classList.contains("ch-day")) {
                target = target.parentElement;
            }

            let index = parseInt($(target).attr("data-index"));
            let filterIndex = this.activeFilters.findIndex(a => a[0] === index);

            if(filterIndex === -1) {
                this.activeFilters.push([index, index + 1]);
                WGL.filterDim('date', 'dateF', this.activeFilters);
            } else {
                this.activeFilters.splice(filterIndex, 1);
                WGL.filterDim('date', 'dateF', this.activeFilters);
            }

            updateFiltersHeader(this.activeFilters.length);

            /*if($(this).hasClass("ch-day-selected")) {

           } else {
               let index = $(this).attr("data-index");
               activeFilters.push([index, index + 1]);
               WGL.filterDim('date', 'dateF', activeFilters);
           }*/
        });

        const filter_clean = $("#chd-container-" + div_id + " .chart-filters-clean")
        filter_clean.off("click").on("click", e => {
            e.stopPropagation();
            that.clearSelection();
        });

        $(".dropdown_arrow").off("click").on("click", () => $(".select-calendar").click());



        /*const filter_clean = $("#chd-container-" + div_id + " .chart-filters-clean");
        filter_clean.off("click");
        filter_clean.on("click", function (e) {
            e.stopPropagation();
            that.clearSelection();
        });

        let div_hm = $("<div id='heatmap-boxes-parent' class='js-heatmap' style='height: 160px; padding-top: 1; background-color: whitesmoke'></div>");
        $("#" + div_id).append(div_hm);

        var width = 900;
        var height = 150;
        var dx = 35;
        var gridClass = 'js-date-grid day';

        var heatmapSvg = d3.select('.js-heatmap')
            .append('svg')
            .style('width', '100%')
            .style('height', '100%')
            .style('background-color', 'whitesmoke')
            .append('svg')
            .attr('width', width)
            .attr('height', height)
            .attr('class', 'color');

        d3.select("body").select("div.tooltip-hm-boxes").remove();

        // Add a grid for each day between the date range.
        const dates = Object.keys(m.data).sort((a,b) => {return new Date(a.split("-").reverse().join("-")) - new Date(b.split("-").reverse().join("-"))});

        let div = d3.select("body")
            .append("div")
            .attr("class", "tooltip tooltip-hm-boxes")
            .style("opacity", 0)
            .style("white-space", "pre-line")
            .html("<span class='total-records'></span><span class='first-text'></span><span class='tooltip-date'></span>, <span class='selected-records'></span><span class='second-text'></span>");
        const rect = heatmapSvg.append('g')
            .attr('transform', `translate(${dx},35)`);

        const months = [];
        const years = [];

        let week_counter = 0;
        let dragStart = null;
        let dragEnd = null;

        let rangeSelection = [];
        let dragCounter = 0;

        const arrayUnique = (array) => {
            var a = array.concat();
            for(var i=0; i<a.length; ++i) {
                for(var j=i+1; j<a.length; ++j) {
                    if(a[i] === a[j])
                        a.splice(j--, 1);
                }
            }

            return a;
        };

        const drag = d3.behavior.drag()
            .on('drag', () => {

                const target = d3.event.sourceEvent.target;
                const index = $(".day").index(target);

                dragCounter++;

                if(dragCounter > 2
                    && dragStart == null) {
                    dragStart = index;
                    return;
                }

                if(dragStart != null) {

                    dragEnd = index;

                    const e = new CustomEvent("rangeselection", {'detail': {dragStart, dragEnd}});
                    const days = document.getElementsByClassName("day");
                    for (let i = 0; i < days.length; i++) {
                        days[i].dispatchEvent(e);
                    }
                }

            })
            .on('dragend', () => {
                dragStart = null;
                dragEnd = null;
                dragCounter = 0;

                if(rangeSelection.length === 0) {
                    return;
                }

                activeFilters = arrayUnique(activeFilters.concat(rangeSelection));

                let id;
                let el;
                for(let i=0; i<activeFilters.length; i++) {
                    id = "date-"+dates[activeFilters[i][0]];
                    el = document.getElementById(id);
                    el.classList.add("heatmap-boxes-selected");
                    el.style.fill = selectedColors[formatColor(m.data[dates[activeFilters[i][0]]])];
                }

                WGL.filterDim('date', 'dateF', activeFilters);
                updateFiltersHeader(activeFilters.length);

                rangeSelection = [];
            });

        rect.selectAll(".days")
            .data(dates)
            .enter()
            .append('rect')
            .attr("id", d => "date-"+d)
            .attr('class', gridClass)
            .attr('width', CELL_SIZE)
            .attr('height', CELL_SIZE)
            .style("stroke", "#fff")
            .style("stroke-width", "3px")
            .attr('x', (d, index) => {
                let dSplit = d.split('-');
                let day = new Date(dSplit[2], dSplit[1]-1, dSplit[0]).getDay();
                if(index !== 0 && day === 0) {
                    week_counter+=1;
                }
                return ((week_counter + 8) * CELL_SIZE)
            })
            .attr('y', (d) => {
                let dSplit = d.split('-');
                let day = new Date(dSplit[2], dSplit[1]-1, dSplit[0]).getDay();
                return (day * CELL_SIZE)
            })
            .attr('data-weekday', (d) => {
                const dSplit = d.split('-');
                const date = (new Date(dSplit[2], dSplit[1]-1, dSplit[0]));
                let day = date.getDay();
                day = (day === 0 ? 6 : day-1);
                return day;
            })
            .attr('title', (d) => {
                let countData = m.data[d];
                let dSplit = d.split('-');
                if(months.indexOf(dSplit[1]-1) === -1) {
                    months.push(dSplit[1]-1)
                }
                if(years.indexOf(dSplit[2]) === -1) {
                    years.push(dSplit[2]);
                }
                const date = d3.timeFormat('%b %d, %Y')(new Date(dSplit[2], dSplit[1]-1, dSplit[0]));
                if (countData.count === 1) return `1 post on ${date}`;
                else return `${countData} posts on ${date}`;
            })
            .on('rangeselection', (d, index) => {

                const target = d3.event.target;

                if(index >= d3.event.detail.dragStart
                    && index <= d3.event.detail.dragEnd) {

                    if(rangeSelection.findIndex( a => a[0] === index) === -1) {
                        rangeSelection.push([index, index+1]);
                        target.classList.add("heatmap-boxes-selected");
                        target.style.fill = selectedColors[formatColor(m.data[d])];
                    }
                } else {
                    let arrayIndex = rangeSelection.findIndex(a => a[0] === index);

                    if(arrayIndex > 0) {
                        rangeSelection.splice(arrayIndex, 1);
                    }
                    target.classList.remove("heatmap-boxes-selected");
                    target.style.fill = getUnselectedColor(m.data[d] / m.maxCount);
                }

                console.log(rangeSelection);

                if(index === dates.length - 1) {
                    WGL.filterDim('date', 'dateF', rangeSelection);
                }
            })
            .on('mouseover', d => {

                $(d3.event.target).css("fill", "#f9cb9c");
                const dSplit = d.split('-');
                let date = d3.timeFormat('%b %d %Y')(new Date(dSplit[2], dSplit[1]-1, dSplit[0]));

                const recordsSelected = WGL._dimensions["date"].readPixels().find( a => a.val === d).selected;

                div.transition()
                    .duration(200)
                    .style("opacity", .9);
                div .style("left", (d3.event.pageX) + "px")
                    .style("top", (d3.event.pageY - 28) + "px");

                if(recordsSelected > 0) {
                    $(".total-records").text(numberWithSpaces(m.data[d]));
                    $(".tooltip-date").text(date);
                    $(".selected-records").text(numberWithSpaces(recordsSelected));
                } else {
                    $(".total-records").text(numberWithSpaces(m.data[d]));
                    $(".tooltip-date").text(date);
                    $(".selected-records").text(0);
                }
            })
            .on('mouseout', (d, index) => {

                const target = d3.event.target;

                if($(target).attr("class").indexOf("heatmap-boxes-selected") > -1
                    || rangeSelection.findIndex( a => a[0] === index) > -1
                    || WGL._dimensions["date"].readPixels().find( a=> a.val === d ).selected > 0) {
                    $(target).css("fill", selectedColors[formatColor(m.data[d])]);
                } else {
                    $(target).css("fill", getUnselectedColor(m.data[d] / m.maxCount));
                }

                div.transition()
                    .duration(500)
                    .style("opacity", 0);
            })
            .on('click', (d, index) => {

                const target = d3.event.target;

                if($(target).attr("class").indexOf("heatmap-boxes-selected") > -1) {
                    activeFilters.splice( activeFilters.findIndex(a => a[0] === index), 1 );
                    WGL.filterDim('date', 'dateF', activeFilters);
                    target.classList.remove("heatmap-boxes-selected");
                    target.style.fill = getUnselectedColor(m.data[d] / m.maxCount);
                } else {
                    activeFilters.push([index, index+1]);
                    WGL.filterDim('date', 'dateF', activeFilters);
                    target.classList.add("heatmap-boxes-selected");
                    target.style.fill = selectedColors[formatColor(m.data[d])];
                }

                updateFiltersHeader(activeFilters.length);

            })
            .attr('date', (d) => d)
            .filter((d) => dates.indexOf(d) > -1)
            .attr('class', (d) => `${gridClass}`)
            .style('fill', (d) => {
                    return getUnselectedColor(m.data[d] / m.maxCount);
                }
            )
            .call(drag);

        // Add year label.
        rect.append('text')
            .attr('transform', `translate(-9,${CELL_SIZE * 3.5})rotate(-90)`)
            .style('text-anchor', 'middle')
            .text(years.join(" / "));

        // Add weekdays' labels.
        rect.append('text')
            .attr('transform', () => {
                return (window.innerWidth <= 1366 ? "translate(340, 12)" : "translate(400, 12)");
            })
            .style('text-anchor', 'middle')
            .attr('class', 'first-wd')
            .text("Sun");

        rect.append('text')
            .attr('transform', () => {
                return (window.innerWidth <= 1366 ? "translate(340, 40)" : "translate(400, 40)");
            })
            .style('text-anchor', 'middle')
            .attr('class', 'second-wd')
            .text("Tue");

        rect.append('text')
            .attr('transform', () => {
                return (window.innerWidth <= 1366 ? "translate(340, 70)" : "translate(400, 70)");
            })
            .style('text-anchor', 'middle')
            .attr('class', 'third-wd')
            .text("Thu");

        rect.append('text')
            .attr('transform', () => {
                return (window.innerWidth <= 1366 ? "translate(340, 98)" : "translate(400, 98)");
            })
            .style('text-anchor', 'middle')
            .attr('class', 'fourth-wd')
            .text("Sat");

        // Render x axis to show months
        rect.selectAll('.month')
            .data(months)
            .enter()
            .append('text')
            .attr('x', (d, index) => {
                return (index * (4.5 * CELL_SIZE) + dx + 100)
            })
            .attr('y', '-10')
            .attr('class', (d, index) => {
                return ("_"+index+'_month')
            })
            .text((d) => d3.timeFormat('%b')(new Date(0, d + 1, 0)));

        if(permalink_input != null) {

            let loadedFilters = getUrlParameter(encodeURIComponent(m.name));
            if(loadedFilters !== "") {
                activeFilters = JSON.parse(loadedFilters);
                WGL.filterDim(m.name, filterId, activeFilters);
                let element;
                for(let i=0; i<activeFilters.length; i++) {
                    element = $(".day")[activeFilters[i][0]];
                    if(element != null) {
                        element.classList.add("heatmap-boxes-selected");
                        element.style.fill = selectedColors[formatColor(m.data[element.getAttribute("date")])];
                    } else {
                        activeFilters.splice(i,1);
                    }
                }
                updateFiltersHeader(activeFilters.length);
            }

            $("#" + div_id).on("chart:update-permalink", (e) => {
                e.stopPropagation();

                let oldURL = $("#"+permalink_input).val();

                if (WGL._dimensions[m.name].filters[filterId].isActive) {
                    let newURL = updateURLParameter(oldURL, encodeURIComponent(m.name), "[[" + WGL._dimensions[m.name].filters[filterId].actual_filtres.join("],[") + "]]");
                    if (oldURL !== newURL) {
                        $("#" + permalink_input).val(newURL);
                    }
                } else if (window.location.href.indexOf(encodeURIComponent(m.name)) !== -1) {
                    let newURL = updateURLParameter(oldURL, encodeURIComponent(m.name), "");
                    if (oldURL !== newURL) {
                        $("#" + permalink_input).val(newURL);
                    }
                }
            });
        }*/

    };

    this.update = () => {

        if($("#heatmap-boxes-parent").length === 0) {
            this.init();
        }

        if(this.activeFilters.length > 0) {
            $(".ch-day").removeClass("ch-day-selected");
            for(let i=0; i<this.activeFilters.length; i++) {
                $("div.ch-day[data-index="+this.activeFilters[i][0]+"]").addClass("ch-day-selected");
            }

            return;
        }

        const selected = WGL.getDimension("date").readPixels().filter(a => a.selected > 0);

        if(selected.length === 0 && WGL.getNumberOfActiveFilters() === 0) {
            $(".ch-day").addClass("ch-day-selected");
            return;
        }

        if(WGL.getDimension("hours").filters["hoursF"].actual_filtres.length > 0
            && (WGL.getDimension("days").filters["daysF"].actual_filtres.length === 0 || WGL.getDimension("days").filters["daysF"].actual_filtres.length === 7)) {
            $(".ch-day").addClass("ch-day-selected");
            return;
        }

        $(".ch-day").removeClass("ch-day-selected");
        for(let i=0; i<selected.length; i++) {
            $("div.ch-day[data-date="+selected[i]['val']+"]").addClass("ch-day-selected");
        }


    };

    this.clean = function () {
        this.clearSelection();
    };

    this.clearSelection = function () {

        $(".ch-day:not(.is-outside-month-start)").addClass("ch-day-selected");

        this.activeFilters.length = 0;
        WGL.filterDim("date", "dateF", this.activeFilters);
        updateFiltersHeader(0);
    };

    const updateFiltersHeader = function (selected) {

        $("#chd-container-" + div_id + " .chart-filters-selected").html(selected);
        if (selected > 0) {
            $("#chd-container-footer-" + div_id + " .chart-filters-selected").html(selected);
            $("#chd-container-footer-" + div_id).removeClass("hide");
        } else {
            $("#chd-container-footer-" + div_id).addClass("hide");
        }

        if ($(".active-filters-container [id^=chd-container-footer]:not(.hide)").length > 0) {
            $(".close-item-filters").removeClass("hide");
            $("#active-filters-placeholder").addClass("hide");
            $(".active-filters-item .bar-item").addClass("bar-item-active");
            $(".active-filters-container").slideDown();
        } else {
            $("#active-filters-placeholder").removeClass("hide");
            $(".close-item-filters").removeClass("hide");
        }

        if($(".link-permalink").length > 0) {
            $(".link-permalink").trigger("permalink:change");
        }
    };
};

