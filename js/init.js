function initMap() {
    mapboxgl.accessToken = "pk.eyJ1IjoiamlyaS1iIiwiYSI6ImNqZmNjajc1MTJjN2cyeG5ycG5lcWhpNHMifQ.d-4wK9BUDPUHq_SRgHYe9g";
    let map = new mapboxgl.Map({
        container: "map",
        style: "mapbox://styles/mapbox/dark-v9",
        center: [13.389, 49.748],
        zoom: 4,
    });

    function projectPoint(lon, lat) {
        let point = map.project(new mapboxgl.LngLat(lon, lat));
        this.stream.point(point.x, point.y);
    }

    $.getJSON("data/NUTS3-inhabitants-by-age.json", (data) => {

        let transform = d3.geo.transform({point:projectPoint});
        this.path = d3.geo.path().projection(transform);

        const canvas = map.getCanvasContainer();

        let div = d3.select("body")
            .append("div")
            .attr("class", "tooltip tooltip-map")
            .style("opacity", 0)
            .html(
                "<div id='region-name'><span>Region name: </span><span class='region-name'></span></div>" +
                "<div id='region-text'><span>Population in selected age groups: </span><span class='region-selected'></span></div>" +
                "<div id='region-total'><span>Total population: </span><span class='region-total'></span></div>");

        const svg = d3.select(canvas)
            .append("svg").attr("id","mapsvg");

        this.lines = svg.selectAll("path")
            .data(data.features)
            .enter()
            .append("path")
            .attr("d", this.path)
            .classed("line-base", true)
            .attr("id",function (d) {
                return "region_"+d.id;
            })
            .on("mouseover", (d, i) => {
                this.mouseover = true;

                div.transition()
                    .duration(200)
                    .style("opacity", .9);
                div .style("left", (d3.event.pageX) + "px")
                    .style("top", (d3.event.pageY - 28) + "px");

                $(".region-name").text(d.properties['NUTS_NAME']);
                $(".region-selected").text(numberWithSpaces(WGL.getDimension("regions").readPixels()[i]['selected']));
                $(".region-total").text(numberWithSpaces(d.properties['TOTAL']));
            })
            .on("mouseout", () => {
                this.mouseover = false;

                div.transition()
                    .duration(500)
                    .style("opacity", 0);
            })
            .on("click", (d) => {
                click_handler(d, d3.event.shiftKey)
            });


        map.off("moveend").on("moveend", updateMap);

        map.off("click").on("click", () =>{
            if (!this.mouseover){
                clean_map();
            }
        });

        initialLoading();
    });

    // Change it back to a pointer when it leaves.
    map.on("mouseleave", "countries", function () {
        map.getCanvas().style.cursor = "";
    });
}

const initialLoading = () => {
    if($(".line-base").length > 0
        && typeof $(".line-base:first").attr("fill") === "undefined"
        && typeof WGL._dimensions !== "undefined") {
            WGL.updateCharts();
            updateMap();
    } else {
        setTimeout(initialLoading, 500);
    }
};

const hideOutSelectionInChart = () => {
    $(".legend-out").parent().parent().hide();
    $(".legend-unselected.legend-scale").attr("title", "Zoom out to all data");
};

const clickOnUnselectedDataInChart = () => {
    if(!$("#ch11.legend-unselected").hasClass("select-legend-scale")) {
        $("#ch11.legend-unselected>i").click();
    }
};

const clickOnSelectedDataInChart = () => {
    if(!$("#ch10.legend-selected").hasClass("select-legend-scale")) {
        $("#ch10.legend-selected>i").click();
    }
};

const updateMap = () => {

    const lines = d3.selectAll("path.line-base");

    let select = 'selected';
    if(!WGL.getDimension("regions").filters["regionsF"].isActive
        && !WGL.getDimension("ages").filters["agesF"].isActive) {
        select = 'unselected'
    }

    let totals = WGL.getDimension("regions").readPixels();
    let sum = totals.reduce((a, b) => a + (b[select] || 0), 0);
    let max = Math.max.apply(Math, totals.map(function(o) { return o[select]; }));
    let mean_value;
    if(select === 'unselected') {
        mean_value = Math.floor(sum / (totals.length));

    } else {
        mean_value = Math.floor(sum / WGL.getDimension("regions").readPixels().filter( a => a['selected'] > 0).length);
    }
    const threshold = Math.floor(0.8 * max);

    lines.attr("d", this.path);
    lines.attr('fill', (d, i) => getColorLevel(totals[i][select], mean_value, threshold));
}

function init() {
    let data = new DataLoader();
    data.load();
}

function vis(data) {

    WGL.init(
        30440,
        "lib/webglayer/",
        "map"
    );

    const det_TA = WGL.utils.array2TA(data.age_totals);
    WGL.getManager().addDataBuffer(det_TA, 1, 'weight', 0, data.max);

    const regions = {data: data.ids, domain: data.ids_enum, name: "regions", type: "ordinal", label: "regions"};
    let regions_dim = WGL.addOrdinalHistDimension(regions);
    WGL.addLinearFilter(regions, 1522, "regionsF");
    regions_dim.setValueData({name: 'weight', min: 0, max:data.max});

    buildCharts(data);

    WGL.registerUpdateFunction(updateMap);

    WGL.initFilters();
    WGL.render();

    WGL.updateCharts();

    $(".hamburger-menu, #navbar-title").off("click").on("click", () => {
        $("#side-navbar-content").animate({width: 'toggle'});
    });

    $("#charts-menu, #charts-title").off("click").on("click", function(){
        $("#right").animate({width: 'toggle'});
    });

    $("#about-section").off("click").on("click", () => {
        $("#legend-win").hide();
        $("#about-win").css("display") === "block" ? $(".overlay").hide() : $(".overlay").show();
        $("#about-win").show();
    });

    $("#legend-section").off("click").on("click", () => {
        $("#about-win").hide();
        $("#legend-win").css("display") === "block" ? $(".overlay").hide() : $(".overlay").show();
        $("#legend-win").show();
    });

    $(".wgl-about-close").click(() => {
        $("#about-win").hide();
        $(".overlay").hide();
    });

    $(".wgl-legend-close").click(() => {
        $("#legend-win").hide();
        $(".overlay").hide();
    });

    $(".overlay").click(e => {

        if($(e.target).hasClass("overlay") || $(e.target).hasClass("win-wrapper")) {
            $("#legend-win").hide();
            $("#about-win").hide();
            $(".overlay").hide();
        }
    });

    hideOutSelectionInChart();
    clickOnUnselectedDataInChart();
}

function buildCharts(data) {
    let charts = [];

    let params = {
        data: data.ages,
        domain: data.agesEnum,
        name: "ages",
        type: "ordinal",
        label: "Ages",
    };

    const paramsChart = {
        rotate_x: true,
        w: 450,
        showSelectedOnly: false,
        classes_legend: ["","","","Click on the 'zoom to' icons to adjust the chart scale to either the selected or all map data"]
    };

    const chd1 = new WGL.ChartDiv(
        "charts",
        "ch1",
        "ages",
        "Ages",
        20
    );

    let dim = WGL.addOrdinalHistDimension(params);
    chd1.setDim(dim);

    WGL.addLinearFilter(
        params,
        20,
        "agesF",
    );

    charts["ages"] = new WGL.ui.StackedBarChart(
        params,
        "ch1",
        "ages",
        "agesF",
        paramsChart
    );

    dim.setValueData({name: 'weight', min: 0, max: 1000000});

    WGL.addCharts(charts);
}

let click_handler = (d, shiftKey) => {

    clickOnSelectedDataInChart();

    let region_id = d.id;

    if (shiftKey) {
        d3.select("#region_"+region_id).classed("line-selected", true).classed("line-unselected", false);

        if(this.isSelectSomething) {
            WGL.exactFilterDim("regions","regionsF", region_id, true);
        } else {
            WGL.exactFilterDim("regions","regionsF", region_id);
        }

        this.isSelectSomething = true;
    } else {
        d3.selectAll(".line-base").classed("line-selected", false).classed("line-unselected", true);
        d3.select("#region_"+region_id).classed("line-selected", true).classed("line-unselected", false);
        this.isSelectSomething = true;

        WGL.exactFilterDim("regions","regionsF", region_id);
    }
};

let clean_map = () => {

    WGL.filterDim("regions","regionsF", []);

    this.isSelectSomething = false;
    d3.selectAll(".line-base").classed("line-selected", false).classed("line-unselected", false);

    updateMap();

    if(!$("#ch11.legend-unselected").hasClass("select-legend-scale")) {
        $("#ch11.legend-unselected>i").click();
    }
};

function getColorLevel(value, mean_value, threshold) {

    const colorsLevel = [ [0,   191,  87], [255, 222,  0], [243, 55,   25] ];

    let col1 = [];
    let col2 = [];
    let rangeval = 0.0;

    if(value >= threshold) {
        return "rgb("+colorsLevel[2][0]+", "+colorsLevel[2][1]+", "+colorsLevel[2][2]+")";
    }

    if(value < mean_value) {
        col1 = colorsLevel[1];
        col2 = colorsLevel[0];
        rangeval = ((value/mean_value) / 0.5);
    } else {
        col1 = colorsLevel[2];
        col2 = colorsLevel[1];
        rangeval = (value/threshold) / 0.3;
    }

    let col = [-1, -1, -1];
    for(let i = 0;i<3;i++){
        col[i] = Math.trunc(Math.abs(col1[i]*rangeval + col2[i]*(1. - rangeval)))
    }
    return "rgb("+col[0]+", "+col[1]+", "+col[2]+")"
}

$(document).ready(() => initMap());