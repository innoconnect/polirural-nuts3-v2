function DataLoader() {
    let instance;

    DataLoader = function () {
        return instance;
    };

    DataLoader.prototype = this;

    instance = new DataLoader();

    instance.constructor = DataLoader;

    instance.load = function () {

    let FILE = "data/NUTS3-inhabitants-by-age.json";


    let agesEnum = [
        "Y_LT5",
        "Y5-9",
        "Y10-14",
        "Y15-19",
        "Y20-24",
        "Y25-29",
        "Y30-34",
        "Y35-39",
        "Y40-44",
        "Y45-49",
        "Y50-54",
        "Y55-59",
        "Y60-64",
        "Y65-69",
        "Y70-74",
        "Y75-79",
        "Y80-84",
        "Y85-89",
        "Y_GE90",
        "UNK"
    ];

    let ids = [];
    let ids_enum = [];
    let ages = [];
    let age_totals = [];

    let max = 0;

    $.getJSON(FILE, myJson => {
        for (let i = 0, features_len = myJson.features.length; i < features_len; i++) {
            const currentProps = myJson.features[i].properties;

            const id = currentProps["NUTS_ID"];
            ids_enum.push(id);

            for(let j=0, agesKeys = agesEnum.length; j<agesKeys; j++) {
                ages.push(agesEnum[j]);

                if(isNaN(currentProps[agesEnum[j]])) {
                    age_totals.push(0);
                } else {
                    age_totals.push(currentProps[agesEnum[j]]);
                    if(currentProps[agesEnum[j]] > max) max = currentProps[agesEnum[j]];
                }






                ids.push(id);
            }


        }

        vis({
            ids,
            ids_enum,
            ages,
            age_totals,
            agesEnum,
            max
        });
    })

    };

    return instance;
}